import { FETCH_TODOS, FETCH_PETS, FETCH_AUDIO } from "../actions/types";

export default (state = "loading", action) => {
  switch (action.type) {
    case FETCH_TODOS:
      return action.payload;
    case FETCH_PETS:
      return action.payload;
    case FETCH_AUDIO:
      return action.payload;
    default:
      return state;
  }
};
