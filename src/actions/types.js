export const FETCH_TODOS = "FETCH_TODOS";
export const FETCH_USER = "FETCH_USER";
export const FETCH_PETS = "FETCH_PETS";
export const FETCH_AUDIO = "FETCH_AUDIO";
