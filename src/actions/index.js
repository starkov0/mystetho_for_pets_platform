import { todosRef, petsRef, audiosRef, audiosStorageRef, authRef, provider } from "../config/firebase";
import { FETCH_TODOS, FETCH_USER, FETCH_PETS, FETCH_AUDIO } from "./types";

// pet
export const addPet = (newPet, uid) => async dispatch => {
  petsRef
    .child(uid)
    .push()
    .set(newPet);
};

export const updatePet = (pet, petId, uid) => async dispatch => {
  petsRef
    .child(uid)
    .child(petId)
    .set(pet);
};

export const removePet = (petId, uid) => async dispatch => {
  petsRef
    .child(uid)
    .child(petId)
    .remove();
};

export const fetchPets = uid => async dispatch => {
  petsRef.child(uid).on("value", snapshot => {
    dispatch({
      type: FETCH_PETS,
      payload: snapshot.val()
    });
  });
};

// audio
export const addAudio = (newAdudio, petId, uid) => async dispatch => {
  audiosRef
    .child(uid)
    .child(petId)
    .push()
    .set(newAdudio);
};

export const removeAudio = (audioId, petId, uid) => async dispatch => {
  audiosRef
    .child(uid)
    .child(petId)
    .child(audioId)
    .remove();
};

export const fetchAudios = (petId, uid) => async dispatch => {
  audiosRef.child(uid).child(petId).on("value", snapshot => {
    dispatch({
      type: FETCH_AUDIO,
      payload: snapshot.val()
    });
  });
};

// audioFile
export const addAudioFile = (name, file) => async dispatch => {
  const fileRef = audiosStorageRef.child(name)
  fileRef.put(file)
    .then(snap => console.log('upload successful', snap))
    .catch(err => console.error('error uploading file', err));
}

export const removeAudioFile = (audioFileId) => async dispatch => {
  const fileRef = audiosStorageRef.child(audioFileId)
  fileRef.delete()
    .then(snap => console.log('remove successful', snap))
    .catch(err => console.error('error removing file', err));
}

// todo
export const addToDo = (newToDo, uid) => async dispatch => {
  todosRef
    .child(uid)
    .push()
    .set(newToDo);
};

export const completeToDo = (completeToDoId, uid) => async dispatch => {
  todosRef
    .child(uid)
    .child(completeToDoId)
    .remove();
};

export const fetchToDos = uid => async dispatch => {
  todosRef.child(uid).on("value", snapshot => {
    dispatch({
      type: FETCH_TODOS,
      payload: snapshot.val()
    });
  });
};

// auth
export const fetchUser = () => dispatch => {
  authRef.onAuthStateChanged(user => {
    if (user) {
      dispatch({
        type: FETCH_USER,
        payload: user
      });
    } else {
      dispatch({
        type: FETCH_USER,
        payload: null
      });
    }
  });
};

export const signIn = () => dispatch => {
  authRef
    .signInWithPopup(provider)
    .then(result => {})
    .catch(error => {
      console.log(error);
    });
};

export const signOut = () => dispatch => {
  authRef
    .signOut()
    .then(() => {
      // Sign-out successful.
    })
    .catch(error => {
      console.log(error);
    });
};
