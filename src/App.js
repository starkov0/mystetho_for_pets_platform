import React, { Component } from "react";
import ToDoList from "./components/ToDoList";
import PetList from "./components/PetList";
import AudioList from "./components/AudioList";
import Record from "./components/Record";
import Home from "./components/Home";
import requireAuth from "./components/auth/requireAuth";
import { BrowserRouter, Route } from "react-router-dom";
import { connect } from "react-redux";
import { fetchUser } from "./actions";
import Header from "./components/Header"
import { Container } from "react-bootstrap";


class App extends Component {
  componentWillMount() {
    this.props.fetchUser();
  }

  render() {
    return (
      <BrowserRouter>
        <div>
          <Header />
          <Container>
            <Route exact path="/" component={Home} />
            <Route path="/petList" component={requireAuth(PetList)} />
            <Route path="/pets/:petId" component={requireAuth(AudioList)} />
            <Route path="/record/:petId" component={requireAuth(Record)} />
          </Container>
        </div>
      </BrowserRouter>
    );
  }
}

export default connect(null, { fetchUser })(App);
