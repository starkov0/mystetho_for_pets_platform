import * as firebase from "firebase";

import { FirebaseConfig } from "../config/keys";
firebase.initializeApp(FirebaseConfig);

const databaseRef = firebase.database().ref();
export const storageRef = firebase.storage().ref()
export const authRef = firebase.auth();

export const todosRef = databaseRef.child("todos");
export const petsRef = databaseRef.child("pets");
export const audiosRef = databaseRef.child("audios");
export const audiosStorageRef = storageRef.child("audios")

export const provider = new firebase.auth.GoogleAuthProvider();
