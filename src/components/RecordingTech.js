var dc = null, dcInterval = null, pc = null, mediaRecorder = null, blob = null;

export function start(id, ibgResultElement) {

    ibgResultElement.textContent = 'Loading...'

    pc = new RTCPeerConnection();

    if (true) {
        dc = pc.createDataChannel('chat');
        dc.onclose = function() {
            clearInterval(dcInterval);
        };
        dc.onopen = function() {
            dcInterval = setInterval(function() {
                var message = ' '; //PING
                dc.send(message);
            }, 300);
        };
        dc.onmessage = function(evt) {
            ibgResultElement.textContent = evt.data;
            console.log(evt.data)
        };
    }

    var constraints = {
        audio: {
            'noiseSuppression': false,
            'autoGainControl': false,
            'echoCancellation': false,
            channelCount: 1,
            sampleRate: 8000,
        },
        video: false
    };

    navigator.mediaDevices.enumerateDevices()
    .then((devices) => {

        var devices_filtered = devices.filter((d) => d.kind === 'audioinput' && (d.label === 'USB audio CODEC Analog Mono'));
        if (devices_filtered.length) {
            constraints['audio']['deviceId'] = devices_filtered[0].deviceId
        }

        navigator.mediaDevices.getUserMedia(constraints).then(function(stream) {
            stream.getAudioTracks().forEach(function(track) {
                pc.addTrack(track, stream)
            });

            record(stream)

            // // listen to the microphone
            // audioCtx = new (window.AudioContext || window.webkitAudioContext)();
            // source = audioCtx.createMediaStreamSource(stream);
            // analyserNode = audioCtx.createAnalyser();
            // source.connect(analyserNode);
            // analyserNode.connect(audioCtx.destination);
            //
            // isDrawing = true;
            // draw()

            return negotiate(id);
        }, function(err) {
            alert('Could not acquire media: ' + err);
        });

    })
}


export function stop(ibgResultElement) {
    // audioCtx.close();

    mediaRecorder.stop()

    // close data channel
    if (dc) {
        dc.close();
    }

    // close transceivers
    // if (pc.getTransceivers) {
    //     pc.getTransceivers().forEach(function(transceiver) {
    //         transceiver.stop();
    //     });
    // }

    // close local audio / video
    pc.getSenders().forEach(function(sender) {
        sender.track.stop();
    });

    // close peer connection
    setTimeout(function() {
        pc.close();
    }, 500);

    ibgResultElement.textContent = '...'
}


function record(stream) {
    mediaRecorder = new MediaRecorder(stream);

    var chunks = [];

     mediaRecorder.onstart = function(){
         console.log('Started, state = ' + mediaRecorder.state);
     };

    mediaRecorder.onstop = function(e) {
        console.log('Stopped, state = ' + mediaRecorder.state);
        blob = new Blob(chunks, { type:'audio/wav' });
    }

    mediaRecorder.ondataavailable = function(e) {
        chunks.push(e.data);
    }

    mediaRecorder.start()
}

export function getBlob() {
  return blob;
}


function negotiate(id) {
    return pc.createOffer().then(function(offer) {
        return pc.setLocalDescription(offer);
    }).then(function() {
        // wait for ICE gathering to complete
        return new Promise(function(resolve) {
            if (pc.iceGatheringState === 'complete') {
                resolve();
            } else {
                function checkState() {
                    if (pc.iceGatheringState === 'complete') {
                        pc.removeEventListener('icegatheringstatechange', checkState);
                        resolve();
                    }
                }
                pc.addEventListener('icegatheringstatechange', checkState);
            }
        });
    }).then(function() {
        var offer = pc.localDescription;

        var codec = 'default';
        if (codec !== 'default') {
            offer.sdp = sdpFilterCodec(codec, offer.sdp);
        }

        return fetch('https://mystetho.marmotte.industries/backend/offer', {
            body: JSON.stringify({
                sdp: offer.sdp,
                type: offer.type,
                dataType: id
            }),
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST'
        });
    }).then(function(response) {
        return response.json();
    }).then(function(answer) {
        console.log("RTC started")
        return pc.setRemoteDescription(answer);
    });
}


function sdpFilterCodec(codec, realSpd){
    var allowed = []
    var codecRegex = new RegExp('a=rtpmap:([0-9]+) '+escapeRegExp(codec))
    var videoRegex = new RegExp('(m=video .*?)( ([0-9]+))*\\s*$')

    var lines = realSpd.split('\n');

    var isVideo = false;
    for(var i = 0; i < lines.length; i++){
        if (lines[i].startsWith('m=video ')) {
            isVideo = true;
        } else if (lines[i].startsWith('m=')) {
            isVideo = false;
        }

        if (isVideo) {
            var match = lines[i].match(codecRegex)
            if (match) {
                allowed.push(parseInt(match[1]))
            }
        }
    }

    var skipRegex = 'a=(fmtp|rtcp-fb|rtpmap):([0-9]+)'
    var sdp = ""

    var isVideo = false;
    for(var i = 0; i < lines.length; i++){
        if (lines[i].startsWith('m=video ')) {
            isVideo = true;
        } else if (lines[i].startsWith('m=')) {
            isVideo = false;
        }

        if (isVideo) {
            var skipMatch = lines[i].match(skipRegex);
            if (skipMatch && !allowed.includes(parseInt(skipMatch[2]))) {
                continue;
            } else if (lines[i].match(videoRegex)) {
                sdp+=lines[i].replace(videoRegex, '$1 '+allowed.join(' ')) + '\n'
            } else {
                sdp += lines[i] + '\n'
            }
        } else {
            sdp += lines[i] + '\n'
        }
    }

    return sdp;
}

function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}
