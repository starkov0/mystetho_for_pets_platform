import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import * as actions from "../actions";
import PetListItem from "./PetListItem";
import Preloader from "./Preloader";
import { Form, Button, Image } from "react-bootstrap";

class PetList extends Component {
  state = {
    addFormVisible: false,
    addPetName: ""
  };

  handleInputChange = event => {
    this.setState({ addPetName: event.target.value });
  };

  handleFormSubmit = event => {
    const { addPetName } = this.state;
    const { addPet, auth } = this.props;
    event.preventDefault();
    addPet({ name: addPetName }, auth.uid);
    this.setState({ addPetName: "" });
    console.log(123123)
  };

  renderAddForm = () => {
    const { addFormVisible, addPetName } = this.state;
    if (addFormVisible) {
      return (
        <Form onSubmit={this.handleFormSubmit}>
          <Form.Group>
            <Form.Label>What is your pet name ?</Form.Label>
            <Form.Control
            value={addPetName}
            onChange={this.handleInputChange}
            id="newPet"
            type="text"
            placeholder="Enter pet name" />
          </Form.Group>
        </Form>
      );
    }
  };

  renderPets() {
    const { data } = this.props;
    const pets = _.map(data, (value, key) => {
      return (
          <PetListItem key={key} petId={key} pet={value} />
      )
    });
    if (!_.isEmpty(pets)) {
      return pets;
    }
    return (
      <div className="center-align">
        <Image
        alt="Nothing was found"
        id="nothing-was-found"
        src="/img/petQuestion.jpeg"
        fluid />
        <h4>You have no pets registered</h4>
        <p>Start by clicking ADD PET button on the top of the screen</p>
      </div>
    );
  }

  componentWillMount() {
    const { auth } = this.props;
    this.props.fetchPets(auth.uid);
  }

  render() {
    const { addFormVisible } = this.state;
    if (this.props.data === "loading") {
      return (
        <div className="center-align">
          <div className="col s4 offset-s4">
            <Preloader />
          </div>
        </div>
      );
    }
    return (
      <div>
        <Button
          variant="primary"
          size="lg"
          onClick={() => this.setState({ addFormVisible: !addFormVisible })}
          block>
          {addFormVisible ? (
            "Stop"
          ) : (
            "Add pet"
          )}
        </Button>
        {this.renderAddForm()}
        {this.renderPets()}
      </div>
    );
  }
}

const mapStateToProps = ({ data, auth }) => {
  return {
    data,
    auth
  };
};

export default connect(mapStateToProps, actions)(PetList);
