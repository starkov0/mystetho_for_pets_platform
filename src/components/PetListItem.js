import React, { Component } from "react";
import { connect } from "react-redux";
import { updatePet } from "../actions";
import { Link } from "react-router-dom";
import { Card, Button, Col, Form } from "react-bootstrap";

class PetListItem extends Component {
  state = {
    updateName: false,
    newPetName: ""
  }

  handleInputChange = event => {
    this.setState({ newPetName: event.target.value });
  };

  handleFormSubmit = event => {
    const { newPetName } = this.state;
    const { updatePet, auth, pet, petId } = this.props;
    pet.name = newPetName
    event.preventDefault();
    updatePet(pet, petId, auth.uid);
    this.setState({ newPetName: "", updateName: false });
  };


  render() {
    const { petId, pet } = this.props;
    const { updateName } = this.state;
    return (
      <span key="petId">
        <Card>
          <Card.Body>

            <Button
              variant="primary"
              onClick={() => this.setState({ updateName: !updateName })}>
                {updateName ? (
                  "Stop update"
                ) : (
                  "Update name"
                )}
            </Button>

            <Card.Title>{pet.name}</Card.Title>

            {updateName ? (
              <Form onSubmit={this.handleFormSubmit}>
                <Form.Group>
                  <Form.Control
                   onChange={this.handleInputChange}
                   type="text"
                   placeholder="Enter pet name" />
                  <Form.Text className="text-muted">
                    What is your pet name ?
                  </Form.Text>
                </Form.Group>
              </Form>
            ) : (
              null
            )}

            <Link to={"/pets/"+petId}>
              <Button variant="primary">View recordings</Button>
            </Link>

          </Card.Body>
        </Card>
      </span>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  return {
    auth
  };
};

export default connect(mapStateToProps, { updatePet })(PetListItem);
