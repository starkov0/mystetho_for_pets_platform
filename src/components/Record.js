import "./ToDoList.css";
import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import * as actions from "../actions";
import ToDoListItem from "./ToDoListItem";
import Preloader from "./Preloader";
import { start, stop, getBlob } from './RecordingTech'
import { Button } from "react-bootstrap";

class Record extends Component {
  state = {
    name:'',
    petType: 'cat',
    isRecording: false,
  };

  constructor(props) {
    super(props);
    this.startRecording = this.startRecording.bind(this)
    this.stopRecording = this.stopRecording.bind(this)
    this.saveRecording = this.saveRecording.bind(this)
  }

  startRecording() {
    this.setState({
      isRecording: true,
      petType: 'cat',
      name: Date.now() + '',
    })
    start('cat', this.refs.h3)
  }

  stopRecording() {
    this.setState({
      isRecording: false
    })
    stop(this.refs.h3)
  }

  saveRecording() {
    var blob = getBlob()
    console.log(blob)

    if (blob) {
      const { name } = this.state;
      const { addAudio, addAudioFile, auth } = this.props;
      addAudio({name: name}, this.props.match.params.petId, auth.uid)
      addAudioFile(this.state.name, blob)

      var reader = new FileReader();
      reader.readAsArrayBuffer(blob)

      reader.onload = function(){
        var arrayBuffer = reader.result;
        var audioContext = new AudioContext();
        audioContext.decodeAudioData(arrayBuffer)
        .then(function(response) {
          console.log('response', response)
          var fs = response['sampleRate']
          var data = Array.prototype.slice.call(response.getChannelData(0))
          console.log(fs)
        })
      }
    }
  }

  render() {
    return (
      <div>
        <h3 ref="h3" id="idd">...</h3>
        { this.state.isRecording ? (
          <Button onClick={this.stopRecording} variant="success">Stop recording</Button>
        ):(
          <Button onClick={this.startRecording} variant="success">Start recording</Button>
        )}
        <div className="to-do-list-container">
          <button onClick={this.saveRecording}
            className="btn-floating btn-large teal darken-4">Save recording</button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ data, auth }) => {
  return {
    data,
    auth
  };
};

export default connect(mapStateToProps, actions)(Record);
