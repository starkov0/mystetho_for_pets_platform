import "./ToDoList.css";
import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import * as actions from "../actions";
import ToDoListItem from "./ToDoListItem";
import Preloader from "./Preloader";
import { start, stop, getBlob } from './RecordingTech'

class ToDoList extends Component {
  state = {
    addFormVisible: false,
    addFormValue: "",
    fname:'',
    mediaRecorder: null,
    petType: '',
    isRecording: false,
  };

  handleInputChange = event => {
    this.setState({ addFormValue: event.target.value });
  };

  handleFormSubmit = event => {
    const { addFormValue } = this.state;
    const { addToDo, auth } = this.props;
    event.preventDefault();
    addToDo({ title: addFormValue }, auth.uid);
    this.setState({ addFormValue: "" });
  };

  renderAddForm = () => {
    const { addFormVisible, addFormValue } = this.state;
    if (addFormVisible) {
      return (
        <div id="todo-add-form" className="col s10 offset-s1">
          <form onSubmit={this.handleFormSubmit}>
            <div className="input-field">
              <i className="material-icons prefix">note_add</i>
              <input
                value={addFormValue}
                onChange={this.handleInputChange}
                id="toDoNext"
                type="text"
              />
              <label htmlFor="toDoNext">What To Do Next</label>
            </div>
          </form>
        </div>
      );
    }
  };

  renderToDos() {
    const { data } = this.props;
    const toDos = _.map(data, (value, key) => {
      return <ToDoListItem key={key} todoId={key} todo={value} />;
    });
    if (!_.isEmpty(toDos)) {
      return toDos;
    }
    return (
      <div className="col s10 offset-s1 center-align">
        <img
          alt="Nothing was found"
          id="nothing-was-found"
          src="/img/nothing.png"
        />
        <h4>You have completed all the tasks</h4>
        <p>Start by clicking add button in the bottom of the screen</p>
      </div>
    );
  }

  componentWillMount() {
    const { auth } = this.props;
    this.props.fetchToDos(auth.uid);
  }

  start1() {
    this.setState({
      isRecording: true,
      petType: 'cat',
      fname: Date.now() + '',
    })
    start('cat', this.refs.h3)
  }

  stop1() {
    this.setState({
      isRecording: false
    })
    stop(this.refs.h3)
  }

  consolelogBlob() {
    var blob = getBlob()
    console.log(blob)

    const { addAudioFile, auth } = this.props;
    addAudioFile(this.state.fname, blob)


    var reader = new FileReader();
    reader.readAsArrayBuffer(blob)

    reader.onload = function(){
      var arrayBuffer = reader.result;
      var audioContext = new AudioContext();
      audioContext.decodeAudioData(arrayBuffer)
      .then(function(response) {
        console.log('response', response)
        var fs = response['sampleRate']
        var data = Array.prototype.slice.call(response.getChannelData(0))
        console.log(fs)
      })
    }
  }

  render() {
    const { addFormVisible } = this.state;
    if (this.props.data === "loading") {
      return (
        <div className="row center-align">
          <div className="col s4 offset-s4">
            <Preloader />
          </div>
        </div>
      );
    }
    return (
      <div>

        <h3 ref="h3" id="idd">...</h3>

        { this.state.isRecording ? (
          <button onClick={this.stop1.bind(this)}
          className="btn-floating btn-large teal darken-4">Stop</button>
        ):(
          <button onClick={this.start1.bind(this)}
          className="btn-floating btn-large teal darken-4">Start</button>
        )}
        <div className="to-do-list-container">


        <button onClick={this.consolelogBlob.bind(this)}
        className="btn-floating btn-large teal darken-4">Blob</button>

          <div className="row">
            {this.renderAddForm()}
            {this.renderToDos()}
          </div>
          <div className="fixed-action-btn">
            <button
              onClick={this.props.signOut}
              id="sign-out-button"
              className="btn-floating btn-large teal darken-4"
            >
              <i className="large material-icons">exit_to_app</i>
            </button>
            <button
              onClick={() => this.setState({ addFormVisible: !addFormVisible })}
              className="btn-floating btn-large teal darken-4"
            >
              {addFormVisible ? (
                <i className="large material-icons">close</i>
              ) : (
                <i className="large material-icons">add</i>
              )}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ data, auth }) => {
  return {
    data,
    auth
  };
};

export default connect(mapStateToProps, actions)(ToDoList);
