import React, { Component } from "react";
import { connect } from "react-redux";
import { completeToDo } from "../actions";
import { Link } from "react-router-dom";
import { Card, Button, Col } from "react-bootstrap";
import { removeAudio, removeAudioFile } from "../actions";

class AudioListItem extends Component {
  handleDeleteClick = (audioId, audio, petId) => {
    const { removeAudio, removeAudioFile, auth } = this.props;
    removeAudio(audioId, petId, auth.uid);
    removeAudioFile(audio.name)
  };

  render() {
    const { audioId, audio, petId } = this.props;
    return (
      <span key="audioId">
        <Card>
          <Card.Body>
            <Card.Title>{audio.name}</Card.Title>
          </Card.Body>
          <Button
            variant="danger"
            onClick={() => this.handleDeleteClick(audioId, audio, petId)}>
            Danger
          </Button>
        </Card>
      </span>
    );
  }
}

const mapStateToProps = ({ data, auth }) => {
  return {
    auth,
    data
  };
};

export default connect(mapStateToProps, { removeAudio, removeAudioFile })(AudioListItem);
