import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import * as actions from "../actions";
import AudioListItem from "./AudioListItem";
import Preloader from "./Preloader";
import { Form, Button, Image } from "react-bootstrap";
import { Link } from "react-router-dom";

class AudioList extends Component {

  renderAudios() {
    const { data } = this.props;
    const audios = _.map(data, (value, key) => {
      return (
          <AudioListItem key={key} audioId={key} petId={this.props.match.params.petId} audio={value} />
      )
    });
    if (!_.isEmpty(audios)) {
      return audios;
    }
    return (
      <div className="center-align">
        <Image
        alt="Nothing was found"
        id="nothing-was-found"
        src="/img/stethoscope.jpeg"
        fluid />
        <h4>You have no heart sounds registered</h4>
        <p>Start by clicking ADD HEART SOUND button on the top of the screen</p>
      </div>
    );
  }

  componentWillMount() {
    const { auth } = this.props;
    this.props.fetchAudios(this.props.match.params.petId, auth.uid);
  }

  render() {
    if (this.props.data === "loading") {
      return (
        <div className="center-align">
          <div className="col s4 offset-s4">
            <Preloader />
          </div>
        </div>
      );
    }
    return (
      <div>
      <Link to={"/record/"+this.props.match.params.petId}>
          <Button
            variant="primary"
            size="lg"
            block>
            "Add Heart Sound"
          </Button>
        </Link>
        {this.renderAudios()}
      </div>
    );
  }
}

const mapStateToProps = ({ data, auth }) => {
  return {
    data,
    auth
  };
};

export default connect(mapStateToProps, actions)(AudioList);
