import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";
import { signIn, signOut } from "../actions";
import { Nav, Navbar, NavItem, Button } from "react-bootstrap";

export class Header extends React.Component {

  render() {
    return (
      <div>
      <Navbar bg="light" >
        <Navbar.Brand href="/">VetStetho</Navbar.Brand>
        <Nav className="mr-auto">
        {this.props.auth ? (
          <Nav.Link href="/petList">Pets</Nav.Link>
        ) : (
          null
        )}
        </Nav>
        <Nav>
          <Nav.Link>
            {this.props.auth ? (
              <Button color="primary" onClick={this.props.signOut}>Sign out</Button>
            ) : (
              <Button color="primary" onClick={this.props.signIn}>Sign in with Google</Button>
            )}
          </Nav.Link>
        </Nav>
      </Navbar>
    </div>
    )
  }
}

const mapStateToProps = ({ auth }) => {
  return {
    auth
  };
};

export default connect(mapStateToProps, {signIn, signOut})(Header);
