import React, { Component } from "react";
import { connect } from "react-redux";
import { addPet } from "../actions";

class RecordAudio extends Component {
  state = {
    audioContext: new AudioContext(),
    pc: new RTCPeerConnection(),
    dc: null,
    dcInterval: null,
    pc: null,
    fname:'',
    mediaRecorder: null,
  };

  for (var i = 0; i<state.drawBuffer.length; i++) {
      state.drawBuffer[i] = 128
  }
  state.canvas.style.width ='100%';
  state.canvas.width  = state.canvas.offsetWidth;
  state.canvas.height = 100;
  WIDTH = state.canvas.width
  HEIGHT = state.canvas.height

  handleInputChange = event => {
    this.setState({ addFormName: event.target.value });
  };

  handleFormSubmit = event => {
    const { addFormName } = this.state;
    const { addToDo, auth } = this.props;
    event.preventDefault();
    addPet({ name: addFormName }, auth.uid);
  };

  renderAddForm = () => {
    const { addFormName } = this.state;
    return (
      <div id="todo-add-form" className="col s10 offset-s1">
        <form onSubmit={this.handleFormSubmit}>
          <div className="input-field">
            <i className="material-icons prefix">note_add</i>
            <input
              value={addFormName}
              onChange={this.handleInputChange}
              id="addPetInput"
              type="text"
            />
            <label htmlFor="addPetInput">What is your pet name ?</label>
          </div>
        </form>
      </div>
    );
  };

  render() {
    const { petId, pet } = this.props;
    return (
      <div key="toDoName" className="col s10 offset-s1 to-do-list-item teal">
        <h4>
          {todo.title}{" "}
          <span
            onClick={() => this.handleCompleteClick(todoId)}
            className="complete-todo-item waves-effect waves-light teal lighten-5 teal-text text-darken-4 btn"
          >
            <i className="large material-icons">done</i>
          </span>
        </h4>
      </div>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  return {
    auth
  };
};

export default connect(mapStateToProps, { completeToDo })(PetListItem);
