import "./SignIn.css";
import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Image } from "react-bootstrap";

export default class Home extends Component {
  static contextTypes = {
    router: PropTypes.object
  };

  render() {
    return (
      <div className="center-align">
        <Image
          alt="Nothing was found"
          id="nothing-was-found"
          src="/img/vet.jpeg"
          fluid />
        <h4>Welcome to MyStetho</h4>
        <p>Login and get access to pet heart sound analysis</p>
      </div>
    );
  }
}
